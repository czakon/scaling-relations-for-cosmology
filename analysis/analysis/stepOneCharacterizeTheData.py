#!/usr/bin/env python2.7
################################################################################# 
# MODIFICATION HISTORY: 
# 04/08/2015 NGC Created (much of this is copied from characterizeClusterDatabase)
#
# USAGE:
# ./stepOneCharacterizeTheData.py
#################################################################################### 
import sys, getopt
from rpy2.robjects.packages import importr
from rpy2.robjects import FloatVector
import rpy2.robjects as robjects
from pymongo import MongoClient
from datetime import date
from astropy.io import fits
from astropy.cosmology.core import FlatLambdaCDM
import numpy as np
import yaml
from numpy import random as npr
from load_data.configuration_files import cosmo_config
from code import interact
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab
from clusters import cluster_tools
from copy import copy
#import pidly
from load_data.configuration_files import directory_files_config as dir_config
from fit_data import linmixerr_tools
from os import path
import pickle
import time

###############################################################
# Databases that should already be included: ##################
###############################################################
#MCXC 
#BCS
#EBCS
#Reflex
#merten2014
#comalit
#RELICS
#okabe2010
#okabe2015
#CCCP
#umetsu2015
#applegate2014
#hst2015
#mantz2010
#union
#ami
#czakon2015
#mmf3 (not included, and not programmed correctly to be used immediately.
#MOVE THIS TO CLUSTERTOOLS
def mpcToArcmin(radiusMpc,redshift):

    return radiusMpc/(cosmo.kpc_proper_per_arcmin(redshift).value*1E-3)

def fgas_wl_covariance_mc(xlog,ylog,xlogerr,ylogerr,pdf_plot=None):

    # Step 1, cycle through the different fits and calculate the covariance using a multi-carlo technique
    # Generate a normal distribution of ylog_err and xlogerr
    nclusters = len(xlog)
    print('\a')
    #fgas_wl_covar = fgas_wl_covariance(xlog,xlogerr,ylog)
    fg_log = ylog - xlog
    fg_logerr = np.sqrt(ylogerr**2.0 + xlogerr**2.0)

    for indx in range(nclusters):
        n_mcmc = 1000
        xlog_mcmc = npr.randn(n_mcmc)*xlogerr[indx] + xlog[indx]
        mg_mcmc = npr.randn(n_mcmc)*ylogerr[indx] + ylog[indx]

        # Make a sample of what I think that the simulated data should look like.
        scale_covar = 1.0
        scale_fgas = 1.0#7.58
        new_data = npr.multivariate_normal([xlog[indx],fg_log[indx]], \
                                           [[xlogerr[indx]**2.0,-scale_covar*xlogerr[indx]**2.0], \
                                            [-scale_covar*xlogerr[indx]**2.0,(fg_logerr[indx]/scale_fgas)**2.0]],n_mcmc)

        xlim = [min(xlog_mcmc),max(xlog_mcmc)]
        plt.clf()
        plt.subplot(2,2,1)
        plt.scatter(xlog_mcmc,mg_mcmc,marker='.')
        #plt.plot(xlim,(fgas_wl_covar[indx]*(xlim - xlog[indx]) + ylog[indx]/xlog[indx]),'r')
        plt.axvline(xlog[indx])
        plt.axhline(ylog[indx])
        plt.title('Monte Carlo Simulation of the Error')
        plt.xlabel(r'$M_{wl}$')
        plt.ylabel(r'$M_{gas}$')

        plt.subplot(2,2,2)
        plt.hist(xlog_mcmc,histtype='step',label=r'$M_{wl}$',normed=True)
        plt.hist(mg_mcmc,histtype='step',label=r'$M_{gas}$',normed=True)
        plt.hist(mg_mcmc-xlog_mcmc,histtype='step',label=r'$f_{gas}$',normed=True)
        tempx = np.linspace(-1.5,1.5,100)

        x_scale = 1#60
        y_scale = 1#0
        f_scale = 1#60
        plt.plot(tempx,x_scale*mlab.normpdf(tempx,xlog[indx],xlogerr[indx]))
        plt.plot(tempx,y_scale*mlab.normpdf(tempx,ylog[indx],ylogerr[indx]))
        plt.plot(tempx,f_scale*mlab.normpdf(tempx,fg_log[indx],fg_logerr[indx]))
        plt.legend()

        plt.subplot(2,2,3)
        plt.scatter(xlog_mcmc,mg_mcmc - xlog_mcmc,marker='.')
        plt.scatter(new_data[:,0],new_data[:,1],marker='.',color='r',alpha=0.5)
        plt.plot(xlim,(-(xlim - xlog[indx]) + ylog[indx] - xlog[indx]),'r')
        plt.axvline(xlog[indx])
        plt.axhline(ylog[indx] - xlog[indx])
        #plt.title('Monte Carlo Simulation of the Error')
        plt.xlabel(r'$M_{wl}$')
        plt.ylabel(r'$f_{gas}$')
        #plt.subplot(2,2,4)
        if pdf_plot is None:
            plt.show()
        else:
            pdf_plot.savefig()
    return #rho_fgas_mwl

def fgas_wl_covariance(mwl,mwl_err,mgas):

    # This can be figured out through standard propagation of errors technique.
    rho_fgas_mwl = -mwl_err**2*np.abs(mgas)/mwl**2

    return rho_fgas_mwl

def verifyConfigurationParameters(clusters,configuration):
    
    if len(configuration['xaxis']['samples']) is not 1:

        print('\nIn it\'s current confguration, this program can only plot one sample on the x-axis at time.\n')
        interact(local=locals())

    else:

        xsample = configuration['xaxis']['samples'][0]['name']
        ysamples = [configuration['yaxis']['samples'][i]['name'] \
                    for i in range(len(configuration['yaxis']['samples']))]
        
    print('Warning: Will default to only using the overdensity specified in the y-axis for now.')
    xproperty = configuration['xaxis']['samples'][0]['property']
    yproperties = [configuration['yaxis']['samples'][i]['property'] for i in range(len(ysamples))]

    if xproperty == 'redshift':
        xaperture = ''
        xquery = xsample + '.' + xproperty + '.bf'
    elif xproperty == 'K0':
        xaperture = ''
        xquery = xsample + '.' + xproperty 
    else:
        xaperture = str(configuration['xaxis']['samples'][0]['overdensity'])
        xquery = xsample + '.' + xproperty + '.'+ xaperture + '.bf'

    yapertures = []
    yqueries = []

    for indx,yproperty in enumerate(yproperties):

        if yproperty != 'redshift':

            yaperture = str(configuration['yaxis']['samples'][indx]['overdensity'])
            yapertures.append(yaperture)
            yqueries.append(ysamples[indx] + '.' + yproperty + '.'+ yaperture + '.bf')

        else:

            yapertures.append('')
            yqueries.append(ysamples[indx] + '.' + yproperty + '.bf')

    allQueries = [{yqueries[i]:{"$gt":0.0}} for i in range(len(yqueries))]
    allQueries.append({xquery:{"$gt":0.0}})

    cutQueries = copy(allQueries)

    print('\nallQueries:\n')
    for query in allQueries:
        print(query)
        
    allSample = clusters.find({"$and":allQueries})
    print('\n' + str(allSample.count()) + ' clusters before applying any cuts...\n')

    if 'cuts' in configuration.keys():

        for x,item in enumerate(configuration['cuts']):
            
            xSample = item.keys()[0]
            xProperty = item[xSample]['property']
            xMin = item[xSample]['min']
            xMax = item[xSample]['max']
            
            #This is for any property that does not have an aperture, could probably make this more general in the future.
            if xProperty != 'redshift':
                xAperture = str(item[xSample]['overdensity'])
                xQuery = xSample + '.' + xProperty + '.'+ xAperture + '.bf'
            else:
                xQuery = xSample + '.' + xProperty + '.bf'
            cutQueries.append({xQuery:{"$gt":xMin}})
            if xMax > xMin:
                cutQueries.append({xQuery:{"$lt":xMax}})
        cutSample = clusters.find({"$and":cutQueries})
        print(cutQueries)
        print('\n' + str(cutSample.count()) + ' clusters after applying all cuts...\n')
    else:
        cutSample = allSample

    if cutSample.count() == 0:
        print('\nWARNING: the parameters that you entered are not valid...please check the entries in the database and try again.\n')
        interact(local=locals())

    if False:
        fit_fgas = True
        #
    if 'cuts' not in configuration.keys():
        run_id = ysamples[0] + '_' + yproperties[0] + yapertures[0] + \
                 '_' + xsample + '_' + xproperty + xaperture
        configuration['cut_id'] = 'No Cuts'
    else:
        csample = configuration['cuts'][0].keys()[0]
        cproperty = configuration['cuts'][0][csample]['property']
        cmin = str(configuration['cuts'][0][csample]['min'])
        configuration['cut_id'] = 'cut_' + csample + '_' + cproperty + '_' + cmin
        run_id = ysamples[0] + '_' + yproperties[0] + yapertures[0] + \
                 '_' + xsample + '_' + xproperty + xaperture + '_' + configuration['cut_id']
    configuration['run_id'] = run_id
    configuration['plot_filename'] = dir_config.dbPlotFolder + \
                'databaseCharacterization_' + date.today().isoformat() + '_' + run_id + '.pdf'

    return allSample, cutSample, configuration

def getSampleData(clusterSample,configuration,saxis,nsample,y_aperture_correction=''):
    #y_aperture_correction : Must be the name of the sample where you want the new data
    #getSampleData(clusterSample,configuration,'xaxis',0,y_aperture_correction=False):

    svalues = []
    svalue_errors = []
    sradii = []
    new_svalues = []
    sredshifts = []

    saperture = str(configuration[saxis]['samples'][nsample]['overdensity'])
    ssample = configuration[saxis]['samples'][nsample]['name']
    sproperty = configuration[saxis]['samples'][nsample]['property']

    if 'fitScale' in configuration[saxis]['samples'][nsample].keys():
        sscale = configuration[saxis]['samples'][nsample]['fitScale']
    else:
        sscale = 1.0

    #Include the factor of E(z) in the axis labels
    if 'ezPower' in configuration[saxis]['samples'][nsample].keys():

        if ('E(z)' not in configuration[saxis]['samples'][nsample]['axisLabel']) and \
           configuration[saxis]['samples'][nsample]['ezPower']!=0.0:

            if configuration[saxis]['samples'][nsample]['ezPower']==1.0:
            
                configuration[saxis]['samples'][nsample]['axisLabel'] = \
                        '$E(z)$' + configuration[saxis]['samples'][nsample]['axisLabel']
                
            else:

                configuration[saxis]['samples'][nsample]['axisLabel'] = \
                        '$E(z)^{' + str(configuration[saxis]['samples'][nsample]['ezPower']) + '}$' + \
                        configuration[saxis]['samples'][nsample]['axisLabel']
            
    else:
        
        configuration[saxis]['samples'][nsample]['ezPower'] = 0.0
            
    for cluster in clusterSample:

        sredshift = cluster['nominal']['redshift']['bf']
        #sredshift = cluster[ssample]['redshift']['bf']
        # Mass values are in units of 1E14/h
        # Radial measurements are in units of Mpc.
        # Remember, that the concentration must be given with respect to the aperture.
        #START HERE! SEPARATE THE DATA EXTRACTION AND THE FITTING
        if sproperty == 'redshift':

            svalue = cluster[ssample][sproperty]['bf']
            svalue_error = svalue*0.0 #This is just temporary code until we incorporate the error correctly.

        elif sproperty == 'K0':

            svalue = cluster[ssample][sproperty]
            svalue_error = svalue*0.0 #This is just temporary code until we incorporate the error correctly.

        else:
            #radius = mpcToArcmin(cluster[xsample]['radius'][xaperture]['bf'],cluster[xsample]['redshift'])
            svalue = cluster[ssample][sproperty][saperture]['bf']* \
                     cosmo.efunc(sredshift)**configuration[saxis]['samples'][nsample]['ezPower']
            svalue_error = cluster[ssample][sproperty][saperture]['sig']['median']* \
                                                cosmo.efunc(sredshift)**configuration[saxis]['samples'][nsample]['ezPower']
        if sproperty == 'ysz':

            svalue*=cosmo.kpc_proper_per_arcmin(cluster[ssample]['redshift']['bf']).value**2.0*1E-6
            svalue_error*=cosmo.kpc_proper_per_arcmin(cluster[ssample]['redshift']['bf']).value**2.0*1E-6
            #sradius = mpcToArcmin(cluster[xsample]['radius'][xaperture]['bf'],cluster[xsample]['redshift'])

        if y_aperture_correction:

            # START HERE! We need to make sure that we have a different sample as the aperture correction.
            print('\nRecalculating ' + ssample + '\'s mass at ' + y_aperture_correction + \
                  '\'s aperture...')

            old_value = {'mass':svalue,'aperture':int(saperture), \
                         'aperture_radius': cluster[ssample]['radius'][saperture]['bf'], \
                         'scale_radius': cluster[ssample]['radius']['scale']['bf'], \
                         'concentration': {'bf': cluster[ssample]['c'][c_delta[ssample]]['bf'], \
                                           'delta':c_delta[ssample]}} 

            # Don't set the aperture here, because it is no longer technically 
            # the same aperture given that the masses 
            # are different.
            
            if not old_value['aperture_radius']:
                print('The aperture radius needs to be set for this cluster.')
                interact(local=locals())

            new_value = {'mass':0.0,'aperture':0, \
                         'aperture_radius': cluster[y_aperture_correction]['radius'][saperture]['bf'], \
                         'scale_radius': cluster[y_aperture_correction]['radius']['scale']['bf'], \
                         'concentration': {'bf': cluster[y_aperture_correction]['c'][c_delta[y_aperture_correction]]['bf'], \
                                           'delta':c_delta[y_aperture_correction]}} 

            new_value,old_value = cluster_tools.aperture_correction( old_value,new_value, cluster[ssample]['redshift'])
            new_value = new_value['mass']
            #END         if y_aperture_correction:
        #if svalue <= svalue_error:
        #    print('Dropping one cluster because the error is larger than the measurement...')
        #    continue
        svalues.append(svalue)
        svalue_errors.append(svalue_error)
        sredshifts.append(sredshift)
        #xradii.append(xradius)

        if y_aperture_correction:
            new_values.append(new_value)

    #Convert to numpy objects
    svalues = np.array(svalues)
    svalue_errors = np.array(svalue_errors)
    sredshifts = np.array(sredshifts)
    goodValues = svalues>svalue_errors

    if sproperty == 'fgas':
        
        print('\nBe careful, the fgas code hasn\'t been completed.\n')
        interact(local=locals())
        fit_fgas = True
        fvalues = yvalues/xvalues

    else:

        fit_fgas = False

    #We will renormalize the masses by 10, see what the does to the uncertainty.
    slog = np.log10(svalues) +  np.log10(sscale)
    slogerr = 0.5*(np.log10(svalues + svalue_errors) - \
                   np.log10(svalues - svalue_errors))

    if fit_fgas:
        flog = np.log10(fvalues)
        flogerr = np.sqrt(ylogerr**2+xlogerr**2)
        fgas_errors = [10**flog - 10**(flog - flogerr),10**(flog+flogerr) - 10**flog]
        fgas_errors = np.array(fgas_errors)
    

    measured_data = {'values':svalues,'value_errors':svalue_errors, \
                     'slog':slog,'slogerr':slogerr,'redshifts':sredshifts,'goodValues':goodValues}

    return measured_data

def characterizeError(sData,pdf_plot=None):

    relError = sData['value_errors']/sData['values']
    sDataHist = np.histogram(sData['slog'])
    zDataHist = np.histogram(sData['redshifts'])
    #Just move this up so that the binning is accurate
    sDataHist[1][-1] += 1.0
    binError = []
    binX = []
    zBinError = []
    zBinX = []
            
    for j in range(len(sDataHist[1])-1):

        yDataBinned = (sData['slog'] >= sDataHist[1][j]) & \
                      (sData['slog'] < sDataHist[1][j+1])
        binX.append(np.mean(sData['values'][yDataBinned]))
        binError.append(np.mean(relError[yDataBinned]))

    sData['redshifts'] = np.array(sData['redshifts'])

    for j in range(len(zDataHist[1])-1):

        zDataBinned = (sData['redshifts'] >= zDataHist[1][j]) & \
                      (sData['redshifts'] < zDataHist[1][j+1])
        zBinX.append(np.mean(sData['redshifts'][zDataBinned]))
        zBinError.append(np.mean(relError[zDataBinned]))

    zBinX = np.array(zBinX)
    plt.scatter(zBinX,zBinError)
    plt.plot(zBinX,0.20*zBinX + 0.35,label='$0.20z+0.35$')
    plt.xlabel('Redshift')
    plt.ylabel('Relative Error')
    plt.legend()
    
    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig()
    plt.clf()
    #plt.scatter(xData['redshifts'],relError)            
    plt.scatter(binX,binError)
    binX.append(30)
    plt.plot(binX,0.55*np.array(binX)**(-0.225),label='$0.55M^{-0.225}$')
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.xlabel('Mass 1E14')
    plt.ylabel('Fractional Error')
    plt.ylim([2E-1,3.0])
    plt.legend()
    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig()

    return

def fitSample(xData,yData,nxSample,nySample,configuration,pickle_overwrite=None):

    mylira = importr('mylira')
    coda = importr('coda')
    xaperture = str(configuration['xaxis']['samples'][nxSample]['overdensity'])
    yaperture = str(configuration['yaxis']['samples'][nySample]['overdensity'])
    xsample = configuration['xaxis']['samples'][nxSample]['name']
    ysample = configuration['yaxis']['samples'][nySample]['name']
    xproperty = configuration['xaxis']['samples'][nxSample]['property']
    yproperty = configuration['yaxis']['samples'][nxSample]['property']
    xscale = configuration['xaxis']['samples'][nxSample]['fitScale']
    yscale = configuration['yaxis']['samples'][nxSample]['fitScale']

    #for_testing_purposes = fgas_wl_covariance_mc(xlog,ylog,xlogerr,ylogerr)

    yvx_picklename = dir_config.srDataFolder + ysample +  yproperty + '_' + \
                     xsample + xproperty + '.p'
    if pickle_overwrite is None:
        pickle_overwrite = False
    
    if path.isfile(yvx_picklename) and not pickle_overwrite:                        

        print('\nPickle file exists, will load from memory...')
        [sr_corr,sr_uncorr, sr_fix,sr_mock_in,post_xvy,all_fits_xvy] = pickle.load(open(yvx_picklename,'rb'))

    else:

        print('\nSaving fit results to: ' + yvx_picklename + '\n')
        print('\nPickle file for the mg-mwl scaling relations either does not exist ' + \
              'or you want to overwrite it, will rerun linmixerr.')

        beta_fix_xvy = 1.0

        xlog = xData['slog'][xData['goodValues'] & yData['goodValues']]
        ylog = yData['slog'][xData['goodValues'] & yData['goodValues']]
        xlogerr = xData['slogerr'][xData['goodValues'] & yData['goodValues']]
        ylogerr = yData['slogerr'][xData['goodValues'] & yData['goodValues']]

        print('let\'s see if you can get mylira to do this!')
        doMyliraFit = True#If this is not set to true, it will do the IDL fit instead.
        if doMyliraFit:

            x = FloatVector(xlog)
            x.threshold = FloatVector(xlog-10*xlogerr)
            y = FloatVector(ylog)
            y.threshold = FloatVector(ylog-10*ylogerr)
            delta = FloatVector([])
            delta.x = FloatVector(xlogerr)
            delta.x.threshold = FloatVector(xlogerr)
            delta.y = FloatVector(ylogerr)
            delta.y.threshold =  FloatVector(ylogerr)
            covariance = FloatVector([])
            covariance.xy = FloatVector(0.0*xlog)
            z = FloatVector(xData['redshifts'][xData['goodValues'] & yData['goodValues']])
            
            #Just putting this in didn\'t work, I don't know why.
            #beta = FloatVector([])
            #beta.YIZ = 1.0
            temp = mylira.mylira(x,y, \
                                 delta.x,delta.y,\
                                 covariance.xy, \
                                 y.threshold,delta.y.threshold,x.threshold,delta.x.threshold,z, \
                                 export=True)
            summary = coda.summary_mcmc(temp.rx2(2).rx2(1))
            #
            #print(summary(temp[[2]][[1]]))
            ##1. Empirical mean and standard deviation for each variable,
            #Keep this code! This is only way that I know how to extract the information.

            x = 0 
            post_xvy = {}
            sr_corr = {}
            shapex = np.shape(temp[1][0])
            len = shapex[0]
            #START HERE.
            sr_corr['alpha'] = {}
            sr_corr['beta'] = {}
            sr_corr['gamma'] = {}
            sr_corr['alpha']['sig'] = {}
            sr_corr['beta']['sig'] = {}
            sr_corr['gamma']['sig'] = {}
            sr_corr['alpha']['bf'] = summary[0][0]
            fixSlope = False
            post_xvy['alpha'] = np.array([float(temp[1][0].rx(i)[0]) for i in range(1,len+1)])
            if fixSlope:
                sr_corr['beta']['bf'] = 1.0#summary[0][1]
                sr_corr['gamma']['bf'] = summary[0][5]#6]
                sr_corr['alpha']['sig']['median'] = summary[0][7]#8]
                sr_corr['beta']['sig']['median'] = 0.0#summary[0][9]
                sr_corr['gamma']['sig']['median'] = summary[0][12]#4]
                post_xvy['beta'] = np.array([1.0 for i in range(x+1,x+len+1)])
            else:
                sr_corr['beta']['bf'] = summary[0][1]
                sr_corr['gamma']['bf'] = summary[0][6]
                sr_corr['alpha']['sig']['median'] = summary[0][8]
                sr_corr['beta']['sig']['median'] = summary[0][9]
                sr_corr['gamma']['sig']['median'] = summary[0][14]
                x += len
                post_xvy['beta'] = np.array([float(temp[1][0].rx(i)[0]) for i in range(x+1,x+len+1)])
            x += len
            post_xvy['sigma'] = np.array([float(temp[1][0].rx(i)[0]) for i in range(x+1,x+len+1)])
            #x += len ; r2 = matrix.rx(robjects.r.matrix(robjects.IntVector(range(x+1,x+1+len)),nrow=10))
        else:
            sr_fix = linmixerr_tools.run_linfitex_idl(xlog,ylog,xlogerr,ylogerr,beta_fix_xvy,run_id=configuration['run_id'])
            [post_xvy,sr_uncorr] = linmixerr_tools.run_linmixerr_iterate(xlog,ylog,xlogerr,ylogerr,run_id=configuration['run_id'])

            [sr_corr,sr_mock_in,all_fits_xvy] =  linmixerr_tools.find_linmixerr_bias(data,scr_uncorr=sr_uncorr,run_id=configuration['run_id'])
            pickle.dump([sr_corr,sr_uncorr,sr_fix,sr_mock_in,post_xvy,all_fits_xvy],open(yvx_picklename,'wb'))


    if doMyliraFit:
        sr_xvy = {'corr':sr_corr}
    else:
        #Summarize all of the fits into a structure
        sr_xvy = {'corr':sr_corr,'uncorr':sr_uncorr, 'fix':sr_fix, 'mock_in':sr_mock_in}

        # First, correct the posteriors
        post_xvy['alpha'] += sr_corr['alpha']['bf'] - sr_uncorr['alpha']['bf']
        post_xvy['beta'] += sr_corr['beta']['bf'] - sr_uncorr['beta']['bf']
        post_xvy['sigma'] += sr_corr['sigma']['bf'] - sr_uncorr['sigma']['bf']

        # Then, shift the fits back to the original values of the xproperty and yproperty
        post_xvy['alpha'] +=  sr_corr['beta']*np.log10(xscale) - np.log(yscale)
        sr_corr['alpha'] += sr_corr['beta']*np.log10(xscale) - np.log(yscale)
        sr_uncorr['alpha'] += sr_uncorr['beta']*np.log10(xscale) - np.log(yscale)
        sr_fix['alpha'] += sr_fix['beta']*np.log10(xscale) - np.log(yscale)
        sr_mock_in['alpha'] += sr_mock_in['beta']*np.log10(xscale) - np.log(yscale)

        sr_xvy = {'corr':sr_corr,'uncorr':sr_uncorr, 'fix':sr_fix, 'mock_in':sr_mock_in}

    #KEEP THIS, FIGURE OUT HOW TO PLOT THE LINMIXERR ITERATIONS.
    #junk = linmixerr_tools.plot_linmix_iterations(all_fits_xvy,sr_xvy['corr'], sr_xvy['uncorr'], sr_xvy['mock_in'],pdf_plot=pdf_plot)

    #Do the same thing, this time with fgas.
    # START HERE! I think that thi code is obsolete.    
    #if fit_fgas:
    if False:
                    
        fvx_picklename = dir_config.srDataFolder + ysample + 'fgas_' + \
                         xsample + xproperty + '.p'
        print(fvx_picklename)

        if path.isfile(fvx_picklename) and not pickle_overwrite:
                        
            print('\nPickle file exists, will load from memory...')

            [sr_corr,sr_uncorr, sr_fix,sr_mock_in, post_fvx, all_fits_fvx] = pickle.load(open(fvx_picklename,'rb'))
            
        else:

            print('\nPickle file for the fg-mwl scaling relations either does not exist ' + \
                  'or you want to overwrite it, will rerun linmixerr.')
            #rho_fvxas_wl =  fgas_wl_covariance(l_masses,l_mass_errors,yvalues)

            beta_fix_fvx = 0.0
            sr_fix = linmixerr_tools.run_linfitex_idl(xlog,ylog,xlogerr,ylogerr,beta_fix_fvx,run_id=run_id)

            [post_fvx,sr_uncorr] = \
                                   linmixerr_tools.run_linmixerr_iterate(xlog,flog,xlogerr,flogerr,xycov=-xlogerr**2,run_id=run_id)

            # This is the same as above, except that the ylogs have been exchanged with xlogs 
            # and now there is covariance.
            measured_data = {'xlog':xlog,'ylog':flog,'xlogerr':xlogerr,'ylogerr':flogerr,'xycovar':-xlogerr**2}
            [sr_corr, sr_mock_in,all_fits_fvx] = \
                                                 linmixerr_tools.find_linmixerr_bias(measured_data,scr_uncorr=sr_uncorr,run_id=run_id)
            pickle.dump([sr_corr, sr_uncorr,sr_fix,sr_mock_in,post_fvx,all_fits_fvx],open(fvx_picklename,'wb'))
                            
        sr_fvx = {'corr':sr_corr,'uncorr':sr_uncorr,'fix':sr_fix,'mock_in':sr_mock_in}

        junk = linmixerr_tools.plot_linmix_iterations(all_fits_fvx,sr_fvx['corr'], sr_fvx['uncorr'], sr_fvx['mock_in'],pdf_plot=pdf_plot)

        #Summarize all of the fits into a structure
        post_fvx['alpha'] += sr_corr['alpha']['bf'] - sr_uncorr['alpha']['bf']
        post_fvx['beta'] += sr_corr['beta']['bf'] - sr_uncorr['beta']['bf']
        post_fvx['sigma'] += sr_corr['sigma']['bf'] - sr_uncorr['sigma']['bf']
                
        # Need to renormalize the fgas values as well...
        post_fvx['alpha'] +=  sr_corr['beta']*np.log10(xscale) - np.log(yscale)
        sr_corr['alpha'] += sr_corr['beta']*np.log10(xscale) - np.log(yscale)
        sr_uncorr['alpha'] += sr_uncorr['beta']*np.log10(xscale) - np.log(yscale)
        sr_fix['alpha'] += sr_fix['beta']*np.log10(xscale) - np.log(yscale)
        sr_mock_in['alpha'] += sr_mock_in['beta']*np.log10(xscale) - np.log(yscale)

        sr_fvx = {'corr':sr_corr,'uncorr':sr_uncorr,'fix':sr_fix,'mock_in':sr_mock_in}

        # Change the post to include the corrections to the best-fit values.
        fvalues = fvalues * xscale
        fgas_errors = np.array(fgas_errors) * xscale
        flog += np.log10(xscale)
        #END OF THE FGAS STUFF
    #START HERE, PUT THESE INTO A BETTER FORMAT
    #CAN PROBABLY MAKE A SEPARATE FUNCTION TO FORMAT THE DATA.
    sfits = {'sr':sr_xvy,'post':post_xvy}
    return sfits

def plotSample(configuration,xData,yData,dataCut=None,sfits=None,pdf_plot=None):

    plt.clf()
    plt.rcParams['font.size'] = 30
    plt.figure(figsize=(12,12))

    # The two lines below have a different format now because we allow multiple samples in the y-axis,
    # but only one sample for the x-axis.
    xValues = xData['values'][xData['goodValues']]
    #print(len(xData['goodValues']))
    #print(len(yData[0]['goodValues']))
    yValues = [yData[i]['values'][xData['goodValues']] for i in range(len(yData))]

    #This is a placeholder for until we change the code to allow multiple x-axes.
    xi = 0
    if 'axisLimScale' in configuration['xaxis']['samples'][xi].keys():
        xLimScale = configuration['xaxis']['samples'][xi]['axisLimScale']
        xLim = [min(xValues)/xLimScale,max(xValues)*xLimScale]
    else:
        xLim = [configuration['xaxis']['samples'][xi]['axisMin'],configuration['xaxis']['samples'][xi]['axisMax']]

    xlabel =  configuration['xaxis']['samples'][xi]['axisLabel']

    #Cycle through each plot with a different y-axis.
    
    for yi in range(len(yData)):

        if 'axisLimScale' in configuration['yaxis']['samples'][yi].keys():
            yLimScale = configuration['yaxis']['samples'][yi]['axisLimScale']
            ylim = [min(yValues[yi][yValues[yi]>0])/yLimScale,max(yValues[yi])*yLimScale]

        else:

            ylim = [configuration['yaxis']['samples'][yi]['axisMin'],configuration['yaxis']['samples'][yi]['axisMax']]

        ylabel =  configuration['yaxis']['samples'][yi]['axisLabel']
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(configuration['cut_id'] + ': ' + str(len(xValues)) + ' clusters',fontsize=35)
        plt.xlim(xLim)
        plt.ylim(ylim)
        plt.gca().set_xscale(configuration['xaxis']['samples'][xi]['axis'])
        plt.gca().set_yscale(configuration['yaxis']['samples'][yi]['axis'])

        if 'refSlope' in configuration['yaxis']['samples'][yi].keys():
            plt.plot(xLim, \
                     10**(np.log10(xLim)*configuration['yaxis']['samples'][yi]['refSlope'] + \
                          configuration['yaxis']['samples'][yi]['refIntercept']), \
                     label = configuration['yaxis']['samples'][yi]['refLabel'],color='b',linestyle='--')
        plt.legend()
        # Plot the posteriors, and the error bars.
        # First, plot the contours for mgas
        if configuration['fitData']:

            [contour_x,contour_y] = linmixerr_tools.get_linmixerr_contours(np.log10(xLim),sfits[yi]['post'])
                
            plt.fill_between(10**contour_x,10**contour_y['2sig']['upper'],10**contour_y['2sig']['lower'], \
                             color='r',alpha=0.5)

            plt.fill_between(10**contour_x,10**contour_y['1sig']['upper'],10**contour_y['1sig']['lower'], \
                             color='b',alpha=0.5)

            plt.plot(xLim,10**(sfits[yi]['sr']['corr']['beta']['bf']*np.log10(xLim) + sfits[yi]['sr']['corr']['alpha']['bf']), \
                     label=r'$\beta_{corr}='+'{:.2f}'.format(sfits[yi]['sr']['corr']['beta']['bf']) + '\pm' + \
                     '{:.2f}'.format(sfits[yi]['sr']['corr']['beta']['sig']['median']) + '$',color='b')

            doMyliraFit = True
            if not doMyliraFit:
                plt.plot(xLim,10**(sfits[yi]['sr']['uncorr']['beta']['bf']*np.log10(xLim) + sfits[yi]['sr']['uncorr']['alpha']['bf']), \
                         label=r'$\beta_{\rm uncorr}='+'{:.2f}'.format(sfits[yi]['sr']['uncorr']['beta']['bf']) + '\pm' + \
                         '{:.2f}'.format(sfits[yi]['sr']['uncorr']['beta']['sig']['median']) + '$',linestyle='--',color='k')

                plt.plot(xLim,10**(sfits[yi]['sr']['fix']['beta']['bf']*np.log10(xLim) + sfits[yi]['sr']['fix']['alpha']['bf']), \
                         label=r'$\beta_{\rm fix}='+'{:.2f}'.format(sfits[yi]['sr']['fix']['beta']['bf'].tolist()) + '$',linestyle=':')
        
        #Not exactly sure why this is relevant to the legend
        if False:
            if xsample[-1]== xsample:
                plt.legend(bbox_to_anchor=(0.45,0.95),borderaxespad=0)

        if False:
        #Keep this stuff below
        #if y_aperture_correction:
            #if xsample != 'sereno2014' and xsample != 'mahdavi2013':
            new_xValues = np.array([i[str(xaperture)]for i in new_xValues])
            plt.scatter(new_xValues,yvalues,label=xsample, \
                        edgecolors=colors[xsample],marker=markers[xsample],facecolors='none')
            #plt.plot([xscale*min(r_masses),yscale*max(r_masses)],[1.0,1.0],'g:',label='1:1')
            #plt.plot([xscale*min(xValues),yscale*max(xValues)], \
                #         [fgas*xscale*min(xValues),fgas*yscale*max(xValues)],'b--')
        if False:
            plt.legend(loc=2)
        plt.axis('on')

        y_aperture_correction = False

        if y_aperture_correction:
            
            new_xValues = np.array([i[str(xaperture)]for i in new_xValues])

            plt.scatter(new_xValues,yvalues/new_xValues,
                        edgecolors=colors[xsample],marker=markers[xsample], \
                        facecolors='none')
        #END if sfits
        #plt.errorbar(10**xlog,10**ylog,xerr=l_mass_errors,yerr=x_mass_errors,fmt='o',color='g')#,label='Mock Data')
        goodx = np.where(yValues[yi] > 0.0)

        plt.errorbar(xValues[goodx],yValues[yi][goodx], \
                     xerr=xData['value_errors'][goodx],yerr=yData[yi]['value_errors'][goodx], \
                     fmt='o',color='g',zorder=1)#,label='Mock Data')

        if dataCut is not None:

            xValues = dataCut['xvalues']
            yvalues = dataCut['yvalues']
            goodx = np.where(yvalues > 0.0)
            
        plt.scatter(xValues[goodx],yValues[yi][goodx],marker='o',color='b',alpha=0.5,s=100,zorder=2)#,label='Mock Data')
        if pdf_plot is None:
            plt.show()
        else:
            pdf_plot.savefig()
        plt.clf()
    #END OF if nclusters                
    if pdf_plot is not None:
        pdf_plot.close()
        print('\a')
        print('\nPlotted to: ' + configuration['plot_filename'])
    print('The program has been running for '+'{:.2f}'.format((time.time()-tic)/60.0)+' minutes....')
    interact(local=locals())
    return

########################## MAIN PROGRAM ################################
def main(argv):

    global tic
    tic = time.time()

    inputYaml = ''

    try:

        opts, args = getopt.getopt(argv,"hi:",["ifile="])

    except getopt.GetoptError:

        print('stepOneCharacterizeTheData.py -i <inputYAML>')
        sys.exit(2)

    for opt, arg in opts:
        
        if opt == '-h':
            print('stepOneCharacterizeTheData.py -i <inputYAML>')
            sys.exit()

        elif opt == '-i':
            inputYaml = arg

    print('\nInput YAML file is: ' + inputYaml + '\n')

    global cosmo
    cosmo = FlatLambdaCDM(H0=cosmo_config.h0*100,Om0=cosmo_config.Omega_M)
    stream = file(inputYaml,'r')
    configuration = yaml.load(stream)
    stream.close()

    # Initialize the port to communicate with the database
    client = MongoClient('mongodb.asiaa.sinica.edu.tw',27000)
    db = client.clusters#Can call db.collection_names
    clusters = db.clusters
    dataCut = None

    allSample , cutSample, configuration = verifyConfigurationParameters(clusters,configuration)
    xData = getSampleData(allSample,configuration,'xaxis',0,y_aperture_correction='')

    if 'cuts' in configuration.keys():
        xCutData = getSampleData(cutSample,configuration,'xaxis',0,y_aperture_correction='')

    pdf_plot = PdfPages(configuration['plot_filename'])

    plt.clf()
    plt.rcParams['font.size'] = 30
    plt.figure(figsize=(12,12))

    doCharacterizeError = False

    if doCharacterizeError:
        junk = characterizeError(xData,pdf_plot=pdf_plot)

    yData = []
    sfits = []
    xi = 0

    for yi in range(len(configuration['yaxis']['samples'])):

        allSample , cutSample, configuration = verifyConfigurationParameters(clusters,configuration)
        if 'cuts' in configuration.keys():
            yiData = getSampleData(cutSample,configuration,'yaxis',yi,y_aperture_correction='')
        else:
            yiData = getSampleData(allSample,configuration,'yaxis',yi,y_aperture_correction='')

        yData.append(yiData)

        if configuration['fitData']:

            if sum(yiData['slogerr']) != 0.0 :

                if 'cuts' in configuration.keys():
                    sfit0 = fitSample(xCutData,yiData,xi,yi,configuration,pickle_overwrite=False)
                else:
                    sfit0 = fitSample(xData,yiData,xi,yi,configuration,pickle_overwrite=False)

                sfits.append(sfit0)

            else:

                print('\nThere are no error bars on the y-values, will skip the fitting....\n')
                sfits.append(None)
        else:
            sfits.append(None)

    if 'cuts' in configuration.keys():
        plotSample(configuration,xCutData,yData,dataCut=dataCut,sfits=sfits,pdf_plot=pdf_plot)
    else:
        plotSample(configuration,xData,yData,dataCut=dataCut,sfits=sfits,pdf_plot=pdf_plot)

    print('End of program')

if __name__ == "__main__":
    main(sys.argv[1:])
