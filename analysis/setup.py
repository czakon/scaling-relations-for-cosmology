#Usage: this code only needs to be run once.
# python setup.py install --user
#
#MODIFICATION HISTORY:
#04/08/2016: Created by Nicole Czakon
#
# USAGE:
# from clusters import cluster_tools as ct
# ct.find_cat_x (finds the intersection of two cluster samples)
from setuptools import setup
from setuptools import find_packages

setup(name='analysis', \
      description = 'this package includes all of the code that is needed to reproduce the analysis.', \
      author='Nicole Czakon', \
      packages=find_packages(), \
      zip_sage=False)


