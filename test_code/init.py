from distutil.core import setup, Extension

module1 = Extension('pidl',
                    include_dirs=['/penguin/apps/idl/idl84/external/include'],
                    library_dirs = ['/penguin/apps/idl/idl84/bin/bin.linux.x86_64'],
                    sources = ['pidl.c'])

setup(name='PackageName',version='1.0',
      description= 'This is an IDL package',ext_modules=[module1])
