#include <Python.h>

#include "idl_export.h"

 

typedef PyObject * PyPtr;

 

static PyPtr PidlError;

 

int AppInitIDL(void)

{

  IDL_INIT_DATA init_data;

  /* Combine any other IDL init options with NOCMDLINE */

  init_data.options = IDL_INIT_NOCMDLINE;

  return IDL_Initialize(&init_data);

}

 

static PyPtr pidl_getvar(PyPtr self, PyPtrargs)

{

  const char *varname;

  PyPtr res;

  IDL_VPTR var;

  IDL_MEMINT i;

  IDL_STRING *s;

  UCHAR *bptr;

  IDL_INT *iptr;

  IDL_LONG *lptr;

  float *fptr;

  double *dptr;

 

  if(!PyArg_ParseTuple(args, "s",&varname)) return NULL;

 

  res=Py_BuildValue("");

  var = IDL_FindNamedVariable(varname,FALSE);

  if(!var) {

  } else if (var->flags & IDL_V_ARR) {

    // array

    switch(var->type) {

    case IDL_TYP_BYTE:

      res = PyTuple_New(var->value.arr->n_elts);

      bptr = (UCHAR *) var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++, bptr++) {

        PyTuple_SetItem(res, i, PyInt_FromLong((long) (*bptr) ));

      }

      break;

    case IDL_TYP_INT:

      res =PyTuple_New(var->value.arr->n_elts);

      iptr = (IDL_INT *)var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++, iptr++) {

        PyTuple_SetItem(res, i, PyInt_FromLong((long) (*iptr) ));

      }

      break;

    case IDL_TYP_LONG:

      res = PyTuple_New(var->value.arr->n_elts);

      lptr = (IDL_LONG *)var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++, lptr++) {

        PyTuple_SetItem(res, i, PyInt_FromLong((long) (*lptr) ));

      }

      break;

    case IDL_TYP_FLOAT:

      res =PyTuple_New(var->value.arr->n_elts);

      fptr = (float*) var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++, fptr++) {

        PyTuple_SetItem(res, i,PyFloat_FromDouble( (double) (*fptr) ));

      }

      break;

    case IDL_TYP_DOUBLE:

      res =PyTuple_New(var->value.arr->n_elts);

      dptr = (double*) var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++, dptr++) {

        PyTuple_SetItem(res, i,PyFloat_FromDouble( *dptr ));

      }

      break;

    case IDL_TYP_STRING:

      res =PyTuple_New(var->value.arr->n_elts);

      s = (IDL_STRING *)var->value.arr->data;

      for(i=0; i<var->value.arr->n_elts;i++,s++) {

	PyTuple_SetItem(res, i,PyString_FromString( IDL_STRING_STR(s) ));

      }

      break;

    default:

      break;

    }

  } else {

    // scalar

    switch(var->type) {

    case IDL_TYP_UNDEF:

      break;

    case IDL_TYP_BYTE:

    case IDL_TYP_INT:

    case IDL_TYP_LONG:

      res=Py_BuildValue("i", IDL_LongScalar(var));

      break;

    case IDL_TYP_FLOAT:

    case IDL_TYP_DOUBLE:

      res=Py_BuildValue("f", IDL_DoubleScalar(var));

    case IDL_TYP_STRING:

      res=Py_BuildValue("s", IDL_VarGetString(var));

      break;

    case IDL_TYP_OBJREF:

      res=Py_BuildValue("s", "<IDL_OBJREF>");

      break;

    default:

      break;

    }

  }

  return res;

}

 

void py_to_idl_var(PyPtr val, int level, constchar *varname)

{

  IDL_VPTR res, tmp;

  char cmd[90], newvar[20];

  int status, i;

 

  PyPtr item;

  if(PyList_Check(val)) {

    sprintf(cmd, "%s = obj_new('list')", varname);

    status = IDL_ExecuteStr(cmd);

    sprintf(newvar, "_$tmp%6.6d",level);

    for (i=0; i<PyList_Size(val); i++) {

      py_to_idl_var( PyList_GetItem(val, i),level+1, newvar );

      sprintf(cmd, "%s->Add, temporary(%s)",varname, newvar);

      status = IDL_ExecuteStr(cmd);

    }

  } else if (PyInt_Check(val)) {

    tmp = IDL_GettmpLong64( PyInt_AsLong(val));

    res = IDL_FindNamedVariable(varname, TRUE);

    IDL_VarCopy(tmp, res);

  } else if (PyFloat_Check(val)) {

    tmp = IDL_GettmpDouble(PyFloat_AsDouble(val) );

    res = IDL_FindNamedVariable(varname, TRUE);

    IDL_VarCopy(tmp, res);

  } else if (PyString_Check(val)) {

    tmp = IDL_StrToSTRING(PyString_AsString(val) );

    res = IDL_FindNamedVariable(varname, TRUE);

    IDL_VarCopy(tmp, res);

  } else {

    tmp = IDL_GettmpLong( -1 );

    res = IDL_FindNamedVariable(varname, TRUE);

    IDL_VarCopy(tmp, res);

  }

}

static PyPtr pidl_setvar(PyPtr self, PyPtrargs)

{

  const char *varname;

  PyPtr val;

  IDL_VPTR idlvar, prev;

 

  if(!PyArg_ParseTuple(args, "sO",&varname, &val)) return NULL;

  py_to_idl_var(val, 0,varname);

  return Py_BuildValue("");

}

 

static PyPtr pidl_exec(PyPtr self, PyPtr args)

{

  const char *command;

  int sts;

 

  if(!PyArg_ParseTuple(args, "s",&command)) return NULL;

  sts = IDL_ExecuteStr(command);

  return Py_BuildValue("i", sts);

}

 

static PyMethodDef PidlMethods[] = {

  {"executeStr",pidl_exec, METH_VARARGS, "Execute an IDLcommand."},

  {"getVar",pidl_getvar, METH_VARARGS, "Get an IDLvariable."},

  {"setVar",pidl_setvar, METH_VARARGS, "Set an IDLvariable."},

  {NULL},

};

 

PyMODINIT_FUNC initpidl(void)

{

  PyPtr m;

  m = Py_InitModule("pidl",PidlMethods);

  if (m == NULL) return;

 

  if(!AppInitIDL()) return;

 

  PidlError = PyErr_NewException("pidl.error", NULL,NULL);

  Py_INCREF(PidlError);

  PyModule_AddObject(m, "error", PidlError);

}
