#!/usr/bin/env python
import subprocess
from code import interact
z = 0.2 #the redshift

subp = subprocess.Popen("idl -quiet -e 'print, lumdist(" + repr(z) + ", /SILENT)'",
                        stderr = subprocess.PIPE,stdout = subprocess.PIPE,shell=True)

(dist, _) = subp.communicate()
dist = float(dist.strip())

print(dist)
interact(local=locals())
