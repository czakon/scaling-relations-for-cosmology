# PURPOSE: make scatter plots with the data provided from access_clusters_from _mongo
# MODIFICATION HISTORY:
# 07/20/2015: NGC Created
###################################################################
import os
import sys
access = '~/repository/sr_cosmo/scaling-relations-for-cosmology/clusters/access_clusters_from_mongo.py'
sys.path.append(os.path.dirname(os.path.expanduser(access)))
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import access_clusters_from_mongo as access
from scipy.optimize import curve_fit

catalog = access.whole_catalog('union')
new_catalog = []
temp = []
n=0
for i in catalog:
	if n==4 or n==5: #Ysz #M
		for j in i:
			temp.append(float(j))
		new_catalog.append(temp)
		#print temp
		temp = []
	n+=1
	
def func(x,a,b):
	return a*x+b
new_catalog[0] = np.array(new_catalog[0])
new_catalog[1] = np.array(new_catalog[1])
print new_catalog[0],new_catalog[1]
#np.log(new_catalog[0]
plt.scatter(np.log(new_catalog[1]),np.log(new_catalog[0]),color='blue') #R #Ysz 
#plt.gca().set_xscale('log')
#plt.gca().set_yscale('log')
popt, pcov = curve_fit(func,np.log(new_catalog[1]),np.log(new_catalog[0]))
print popt
#print pcov

plt.plot(np.log(new_catalog[1]),func(np.log(new_catalog[1]),popt[0],popt[1]),color='green')
plt.show()
'''
fig = pl.figure(0)
pl.scatter(mp_x["REDSHIFT"],mp_x["MASS_500"],color='black',s=10,label=r'MCXC$\times$ PLANCK')
pl.scatter(mb_x["REDSHIFT"],mb_x["MASS_500"],color='red',s=40,facecolor='none',linewidth=2,\
           label=r'MCXC$\times$ BOLOCAM')
pl.scatter(mpb_x["REDSHIFT"],mpb_x["MASS_500"],color='green',s=40,facecolor='none',linewidth=2,\
           label=r'MCXC$\times$ PLANCK$\times$ BOLO')
pl.legend(loc='lower right',scatterpoints=1)#['MCXC','MCXC$\times$BOLO'
pl.gca().set_yscale('log')
pl.xlim([0.0,1.0])

pl.ylim([0.1,30.0])
fsize=20
pl.xlabel('Redshift',fontsize=fsize)
pl.ylabel('$M_{500}|L_{500}\ [10^{14}M_{\odot}]$ (Arnaud2010) ',fontsize=fsize)
pl.savefig('planck_m_z.eps',format='eps')
'''
