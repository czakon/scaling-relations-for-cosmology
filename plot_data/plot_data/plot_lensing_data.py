# PURPOSE: make scatter plots with the lensing data provided from access_clusters_from _mongo
#
# MODIFICATION HISTORY:
# 11/10/2015: NGC Created
###################################################################
from code import interact
#
# EXAMPLE 1: Print the mass files that are included in the catalog.
#from load_data.configuration_files import mass_files_config as mass_files
#print(mass_files.CLASH_catalog)
#


interact(local=locals())
