#Usage: this code only needs to be run once.
# python setup.py install --user
#MODIFICATION HISTORY:
#07/20/2015: Created by Nicole Czakon
#
#      packages=['plot_data']
from setuptools import setup
from setuptools import find_packages

setup(name='plot_data', \
      description = 'this package plots the data from load_data.', \
      author='Nicole Czakon', \
      packages=find_packages(), \
      zip_sage=False)


