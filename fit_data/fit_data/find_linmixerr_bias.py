#!/usr/bin/env python
# Purpose:
# Insert a given best-fit value and iterate linmixerr until the bias is accurately characterized.
# Describe what this program does.
# To Do: 
# 1. Simulate this with a given set of X-values and errors
#
import subprocess
from code import interact
import numpy as np
from numpy import random as npr
from matplotlib import pyplot as plt
from astropy.io import fits
import pidly
from matplotlib.ticker import FormatStrFormatter
from copy import copy
from matplotlib.backends.backend_pdf import PdfPages
from datetime import date
import time
import pickle
from fit_data import linmixerr_tools
#
# PROGRAMS:
#
# GENERATING MOCKS:
# generate_mock_catalog
#
# RUNNING IDL:
# run_linmix_err_idl
#
# SAVING TO FITS:
# save_data_pts_to_file
# save_big_data_to_file
#
# CORRECTING THE BEST-FITS
# linmixerr_bias_correction
#
# PLOTTING:
# plot_mock_catalog
# plot_linmix_err_idl
# plot_linmix_iterations
# plot_linmixerr_contours
# plot_final_linmixerr_bias
#

#This is obsolete and has been replaced with linmixerr_tools
#def run_linmix_err_idl(scr_in,file_string):
#    idl = pidly.IDL()
#    output = idl.func('characterize_linmixerr',scr_in['beta'],scr_in['alpha'],scr_in['sigma'])
    #print('\nfinished with run_linmix_err_idl\n')
    
#    return output

def save_data_pts_to_file(xlog,ylog,xlogerr,ylogerr,scr_in,filename):
    
    ## First, create a header, which we will save to the primary HDU
    prihdr = fits.Header()
    prihdr['npts'] = scr_in['npts']
    prihdr['beta'] = scr_in['beta']
    prihdr['alpha'] = scr_in['alpha']
    prihdr['sigma'] = scr_in['sigma']
    prihdr['xrange'] = scr_in['xrange']
    prihdr['xerr_m'] = scr_in['xerr_m']
    prihdr['xerr_std'] = scr_in['xerr_std']
    prihdr['yerr_m'] = scr_in['yerr_m']
    prihdr['yerr_std'] = scr_in['yerr_std']

    prihdu = fits.PrimaryHDU(header = prihdr)

    ## Then, insert that data here.
    col1 = fits.Column(name = 'xlog', format = 'F', array = xlog)
    col2 = fits.Column(name = 'ylog', format ='F', array = ylog)
    col3 = fits.Column(name = 'xlogerr', format ='F', array = xlogerr)
    col4 = fits.Column(name = 'ylogerr', format = 'F', array = ylogerr)
    cols = fits.ColDefs([col1, col2, col3, col4])

    tbhdu = fits.BinTableHDU.from_columns(cols)

    tbhdulist = fits.HDUList(([ prihdu, tbhdu]))
    tbhdulist.writeto(filename, clobber = True)

    return

def linmixerr_bias_correction(scr_uncorr,big_pickle_out):

    #Load pickle file
    [all_scr_in_a, all_scr_in_b, all_scr_in_s,all_scr_out_a, all_scr_out_b, all_scr_out_s,scr_uncorr] = \
                                                                      pickle.load(open(big_pickle_out,"rb"))

    # Find the closest match
    a_x = np.abs(all_scr_out_a[:,0,0] - scr_uncorr['alpha']).argmin()
    a_match = all_scr_out_a[a_x,0,0]

    b_x = np.abs(all_scr_out_b[0,:,0] - scr_uncorr['beta']).argmin()
    b_match = all_scr_out_b[0,b_x,0]

    s_x = np.abs(all_scr_out_s[0,0,:] - scr_uncorr['sigma']).argmin()
    s_match = all_scr_out_s[0,0,s_x]

    if (np.abs(a_match - scr_uncorr['alpha']) > scr_uncorr['delta_a']) or \
       (np.abs(b_match - scr_uncorr['beta']) > scr_uncorr['delta_b']) or \
       (np.abs(s_match - scr_uncorr['sigma']) > scr_uncorr['delta_s']):

        print(['alpha:','{:.2f}'.format(all_scr_out_a[:,0,0].min()), \
               '{:.2f}'.format(all_scr_out_a[:,0,0].max()), \
               '{:.2f}'.format(scr_uncorr['alpha'])])
        print(['beta:','{:.2f}'.format(all_scr_out_b[0,:,0].min()), \
               '{:.2f}'.format(all_scr_out_b[0,:,0].max()), \
               '{:.2f}'.format(scr_uncorr['beta'])])
        print(['sigma:','{:.2f}'.format(all_scr_out_s[0,0,:].min()), \
               '{:.2f}'.format(all_scr_out_s[0,0,:].max()), \
               '{:.2f}'.format(scr_uncorr['sigma'])])

        print('\nYour fit is beyond the range probed by our mock samples, ' + \
              'please extend the range of the tests to include your best fit.')
        interact(local=locals())

    scr_corr = copy(scr_uncorr)
    scr_corr['alpha'] = scr_uncorr['alpha'] + (all_scr_in_a[a_x,0,0] - a_match)
    scr_corr['beta'] = scr_uncorr['beta'] + (all_scr_in_b[0,b_x,0] - b_match)
    scr_corr['sigma'] = scr_uncorr['sigma'] + (all_scr_in_s[0,0,s_x] - s_match)
    print("\n[a_out, a_in, a_uncorr, a_corr]")
    print(['{:.2f}'.format(a_match),'{:.2f}'.format(all_scr_in_a[a_x,0,0]), \
           '{:.2f}'.format(scr_uncorr['alpha']),'{:.2f}'.format(scr_corr['alpha'])])
    print("\n[b_out, b_in, b_uncorr, b_corr]")
    print(['{:.2f}'.format(b_match),'{:.2f}'.format(all_scr_in_b[0,b_x,0]), \
           '{:.2f}'.format(scr_uncorr['beta']),'{:.2f}'.format(scr_corr['beta'])])
    print("\n[s_out, s_in, s_uncorr, s_corr]")
    print(['{:.2f}'.format(s_match),'{:.2f}'.format(all_scr_in_s[0,0,s_x]), \
           '{:.2f}'.format(scr_uncorr['sigma']),'{:.2f}'.format(scr_corr['sigma'])])

    return scr_corr

def plot_mock_catalog(xlog, ylog, xlogerr, ylogerr, scr_in, linmix_data):

    scale_x = 0.5
    xlims = np.array([ min(xlog) - scale_x/2.0, max(xlog) + scale_x])
    alpha = linmix_data.data['alpha']
    beta = linmix_data.data['beta']

    step_x = 0.01
    contour_x = np.arange(xlims[0],xlims[1]+step_x,step_x)

    y_1sig_upper = []
    y_1sig_lower = []
    y_2sig_upper = []
    y_2sig_lower = []

    npts = float(len(beta))
    one_sigma = npts*np.array([0.1586,0.8413])
    two_sigma = npts*np.array([0.0227,0.9772])

    for temp_x in contour_x:
        temp_y = beta*temp_x + alpha
        temp_y.sort()        
        y_1sig_upper.append(temp_y[int(one_sigma[1])])
        y_1sig_lower.append(temp_y[int(one_sigma[0])])
        y_2sig_upper.append(temp_y[int(two_sigma[1])])
        y_2sig_lower.append(temp_y[int(two_sigma[0])])
        
    plt.figure()
    plt.fill_between(contour_x,y_2sig_upper,y_2sig_lower,color='r',alpha=0.5)
    plt.fill_between(contour_x,y_1sig_upper,y_1sig_lower,color='b',alpha=0.5)
    plt.errorbar(xlog,ylog,xerr=xlogerr,yerr=ylogerr,fmt='o',label='Mock Data')
    plt.plot(xlims,scr_in['beta']*xlims + scr_in['alpha'],label='Input SR')
    plt.xlabel('Mock X Data')
    plt.ylabel('Mock Y Data')
    plt.title('Mock random sample for linmixerr.')
    plt.xlim(xlims)
    plt.legend()

    return

def plot_linmix_err_idl(scr_in,fileout):

    hdulist = fits.open(fileout)
    linmix_data = hdulist[1]
    alpha = linmix_data.data['ALPHA']
    beta = linmix_data.data['BETA']
    sigsqr = linmix_data.data['SIGSQR']

    plt.figure()
    scale_x = 0.5
    med_x = np.median(alpha)
    xlims = np.array([min(alpha)-scale_x,max(alpha)+scale_x])
    scale_y = 0.5
    med_y = np.median(beta)
    ylims = np.array([min(beta)-scale_y,max(beta)+scale_y])
    plt.xlim(xlims)
    plt.ylim(ylims)

    plt.plot(xlims,[scr_in['beta'],scr_in['beta']])#,label='Input SR')
    plt.plot([scr_in['alpha'],scr_in['alpha']],ylims)#,label='Input SR')
    plt.plot(xlims,[med_y,med_y],'r--')#,label='Input SR')
    plt.plot([med_x,med_x],ylims,'r--')#,label='Input SR')
    plt.scatter(alpha,beta,marker='.')
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$\beta$')
    plt.title('Linmixerr Results.')

    return linmix_data

def plot_linmix_iterations(all_fits,scr_in,scr_out):

    all_keys = ['alpha','beta','sigma']
    key_labels = [r'$\alpha$',r'$\beta$',r'$\sigma$']

    f, all_x = plt.subplots(1,3,sharey=False,figsize=(9,6))
    plt.suptitle('  Output from mock fitting')#,sharex=False,

    for item in range(len(all_keys)):

        all_x[item].ticklabel_format(style='sci',axis='x',scilimits=(-1,1))
        all_x[item].xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        all_x[item].locator_params(axis='x',nbins=4)
        hist_out = all_x[item].hist(all_fits[all_keys[item]])
        all_x[item].set_xlabel(key_labels[item])
        item_in = scr_out[all_keys[item]]
        item_bf = scr_in[all_keys[item]]
        all_x[item].axvline(item_in,color='b',ls='--')
        all_x[item].axvline(item_bf,color='r',ls='-.')
        all_vals = np.append([item_in,item_bf],all_fits[all_keys[item]])
        x_min = min(all_vals)
        x_max = max(all_vals)
        x_delta = 0.1*(x_max - x_min)
        all_x[item].set_xlim([x_min-x_delta,x_max+x_delta])
        y_max = max(hist_out[0])*1.5
        all_x[item].set_ylim([0,y_max])

    pdf_plot.savefig()

    return 

def plot_linmixerr_contours(all_scr_in,all_scr_out,in_titles,out_titles):

    # First find how different intrinsic scatter values were evaluated.
    nsteps = np.shape(all_scr_in[0])[2]

    for var_x in range(len(all_scr_out)):

        var_a = 0
        var_b = 1
        var_s = 2
    
        for step in range(nsteps):

            plt.clf()
            z_in = all_scr_in[var_s][:,:,step]
            if z_in.min() == z_in.max():
                z_in = z_in.max()
            else:
                print('All of the z ins are supposed to be the same value for the contour plots, '+ \
                      'please check this and make sure that the arrays are in the correct format for your purposes.')
                interact(local=locals())

            plot_title = in_titles[var_s] + ' = ' + str(z_in) 

            delta_z = all_scr_out[var_x][:,:,step] - all_scr_in[var_x][:,:,step]

            plt.contourf(all_scr_in[var_a][:,:,step],all_scr_in[var_b][:,:,step],delta_z)
            plt.xlabel(in_titles[var_a])
            plt.ylabel(in_titles[var_b])
            plt.title(plot_title)
            cbar = plt.colorbar()
            cbar.ax.set_ylabel(out_titles[var_x])
            pdf_plot.savefig()

    return

def plot_final_linmixerr_bias(big_pickle_out):

    # Load the output pickle file
    [all_scr_in_a, all_scr_in_b, all_scr_in_s,all_scr_out_a, all_scr_out_b, all_scr_out_s,scr_uncorr] = pickle.load(open(big_pickle_out,"rb"))

    all_scr_in = [all_scr_in_a, all_scr_in_b, all_scr_in_s]
    all_scr_out = [all_scr_out_a, all_scr_out_b, all_scr_out_s]

    in_titles = [r'Intercept: $\alpha_{in}$',r'Slope: $\beta_{in}$',r'Intrinsic Scatter: $\sigma_{in}$']
    out_titles = [r'$\Delta$ Intercept: $\alpha_{out} - \alpha_{in}$', \
                  r'$\Delta$ Slope: $\beta_{out}-\beta{in}$', \
                  r'$\Delta$ Intrinsic Scatter: $\sigma_{out}-\sigma_{in}$']

    # Cycle through the various values of the intrinsic scatter first
    plot_linmixerr_contours(all_scr_in,all_scr_out,in_titles,out_titles)

    return

##################################################################################
############################ MAIN PROGRAM ########################################
##################################################################################

#print('\nFinished plotting in '+plot_filename)
print('Finished running find_linmixerr_bias')
pdf_plot.close()
print('The program took '+'{:.2f}'.format((time.time()-tic)/60.0)+' minutes to execute.')
#interact(local=locals())
'''
    junk = save_big_data_to_file(all_scr_in_a, all_scr_in_b, all_scr_in_s, \
                                 all_scr_out_a, all_scr_out_b, all_scr_out_s, \
                                 scr_uncorr, big_fileout)
#I am too lazy to figure out how to cast the 3D into a fits file...
def save_big_data_to_file(all_scr_in_a,all_scr_in_b,all_scr_in_s,all_scr_out_a,all_scr_out_b,all_scr_out_s,scr_uncorr,filename):
    #final_scr_out = {'alpha_in':all_scr_in_a,'beta_in':all_scr_in_b,'sigma_in':all_scr_in_s, \
    #                 'alpha_out':all_scr_out_a,'beta_out':all_scr_out_b,'sigma_out':all_scr_out_s}

    ## First, create a header, which we will save to the primary HDU
    prihdr = fits.Header()
    prihdr['npts'] = scr_uncorr['npts']
    prihdr['beta'] = scr_uncorr['beta']
    prihdr['alpha'] = scr_uncorr['alpha']
    prihdr['sigma'] = scr_uncorr['sigma']
    prihdr['xrange'] = scr_uncorr['xrange']
    prihdr['xerr_m'] = scr_uncorr['xerr_m']
    prihdr['xerr_std'] = scr_uncorr['xerr_std']
    prihdr['yerr_m'] = scr_uncorr['yerr_m']
    prihdr['yerr_std'] = scr_uncorr['yerr_std']

    prihdu = fits.PrimaryHDU(header = prihdr)

    ## Then, insert that data here.
    col1 = fits.Column(name = 'alpha_in', format = 'F', array = all_scr_in_a)
    col2 = fits.Column(name = 'beta_in', format ='F', array = all_scr_in_b)
    col3 = fits.Column(name = 'sigma_in', format ='F', array = all_scr_in_s)
    col4 = fits.Column(name = 'alpha_out', format = 'F', array = all_scr_out_a)
    col5 = fits.Column(name = 'beta_out', format ='F', array = all_scr_out_b)
    col6 = fits.Column(name = 'sigma_out', format ='F', array = all_scr_out_s)

    print('There\'s a problem here that you need to figure out...')
    interact(local=locals())

    cols = fits.ColDefs([col1, col2, col3, col4, col5, col6])

    tbhdu = fits.BinTableHDU.from_columns(cols)
    
    tbhdulist = fits.HDUList(([ prihdu, tbhdu]))
    tbhdulist.writeto(filename, clobber = True)

    return
'''
