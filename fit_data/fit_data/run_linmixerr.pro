; This is a scrip to run the linmixerr program
; Author: Nicole G Czakon
; Date Created: 11/22/2015

; Step 1, Read in the data from the file.

function run_linmixerr,filename
                           
    ; By default, this program will save the median best-fit value per iteration
    ;
    ; FOR DIAGNOSTIC PURPOSES:
    ; This saves all of the information from linmix_err.
    ; These files are very large...
    save_mcmc = 0

    ;@~/repository/sr_cosmo/load_data/load_data/configuration_files/directory_files_config.pro

    ;file_string = 'beta_' + strtrim(string(beta,format='(f4.2)'),1) + $
    ;              '_alpha_' + strtrim(string(alpha,format='(f4.2)'),1) + $
    ;              '_sigma_' + strtrim(string(sigma,format='(f4.2)'),1)

    ; This loads the mock_cat_folder
    ;filename = mock_cat_folder + 'sr_' + file_string + '.fits'
    
    if save_mcmc then begin

        fileout = mock_cat_folder + 'bf_' + file_string + '.fits'
        savfile_out = mock_cat_folder + 'bf_beta_' + strtrim(string(beta,format='(f4.2)'),1) + $
                      '_alpha_' + strtrim(string(alpha,format='(f4.2)'),1) + $
                      '_sigma_' + strtrim(string(sigma,format='(f4.2)'),1) + '.sav'
    endif

    ; The /exten indicates to open the exten=1
    print,'Reading in filename: ' + filename
    result = mrdfits(filename,1)
    xlog = result.xlog
    xlogerr = result.xlogerr
    ylog = result.ylog
    ylogerr = result.ylogerr
    xycov = result.xycov

    ; And now, finally, we can insert this into linmixerr.
    linmix_err_2011,xlog,ylog,post,xsig=xlogerr,ysig=ylogerr,xycov=xycov,/metro,/silent

    if save_mcmc then begin
       save,post,filename = savfile_out
       mwrfits,post,fileout
    endif

    return,post
END
           



