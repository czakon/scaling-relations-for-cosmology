; This is a scrip to run the linfitex_mpfit program
; Author: Nicole G Czakon
; Date Created: 12/2/2015

; Step 1, Read in the data from the file.

function run_linfitex,filename

    ; The /exten indicates to open the exten=1
    print,'Reading in filename: ' + filename
    result = mrdfits(filename,0,header)
    header = header[4]
    indx = strsplit(header,'.')
    betafix = strmid(header,indx[-1]-2,3)
    print,'Fixing the slope at: ' + betafix
    if betafix eq 0.0 then betafix=1E-5
    
    betafix = float(betafix)
    result = mrdfits(filename,1)
    xlog = result.xlog
    xlogerr = result.xlogerr
    ylog = result.ylog
    ylogerr = result.ylogerr

    ; And now, finally, we can insert this into linfitex_mpfit.
    start_params = [0.0,1.0]
    print,'Fixing the slope at: ' + string(betafix)
    lf_gls,xlog,ylog,start_params,sr_bf,nis,XSE=xlogerr,YSE=ylogerr,slope_fix=betafix
    ;sr_bf = {slope:slope_fix,intercept:SRfit[0],scatter:sigma_i,d_slope:0.0,d_intercept:p[0]} $
    print,'Best-fit slope' + string(sr_bf.slope)
    return,sr_bf

END
           



