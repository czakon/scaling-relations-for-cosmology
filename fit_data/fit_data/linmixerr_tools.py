# 11/25/2015 Created by Nicole G. Czakon
import subprocess
from code import interact
import numpy as np
from numpy import random as npr
from matplotlib import pyplot as plt
from load_data.configuration_files import directory_files_config as dir_config
#from configuration_files import directory_files_config as dir_config
from astropy.io import fits
from matplotlib.ticker import FormatStrFormatter
from copy import deepcopy
from matplotlib.backends.backend_pdf import PdfPages
from datetime import date
import time
import pickle
#import pidly
#
# FUNCTIONS:
#
# generate_mocks_from_measured_data(measured_data,scr_in=None)
# find_linmixerr_bias(measured_data,scr_uncorr=False,run_id='')
# find_linmixerr_uncertainty(post)
# run_linmixerr_iterate(xlog,ylog,xlogerr,ylogerr,xycov=None,niter=5,run_id='')
# run_linmixerr_idl(xlog,ylog,xlogerr,ylogerr,xycov=None,run_id='')
# run_linfitex_idl(xlog,ylog,xlogerr,ylogerr,run_id='')
# get_linmixerr_contours(xlims,post)
# generate_mock_catalog(scr_in = None)
# save_data_pts_to_file(xlog,ylog,xlogerr,ylogerr,scr_in,filename)
# linmixerr_bias_correction(scr_uncorr,big_pickle_out)
# plot_linmix_err_idl(scr_in,fileout,pdf_plot=None)
# plot_final_linmixerr_bias(big_pickle_out)
# plot_linmix_iterations(all_fits,sr_corr, sr_uncorr, sr_mock_in,pdf_plot=None)
#
# The first couple of functions are ones that have been demonstrated to work
# when calling from outside programs. Following these are programs that have
# yet to be converted.
def generate_mocks_from_measured_data(measured_data,scr_in = None):
    
    nclusters = len(measured_data['xlog'])
    mock_xlog = measured_data['xlog']*0.0
    mock_ylog = measured_data['ylog']*0.0

    for indx in range(nclusters):

        #First find another mock_y according to the scaling relations.
        mock_xlog[indx],mock_ylog[indx] = npr.multivariate_normal([measured_data['xlog'][indx],0.0], \
                                                                  [[measured_data['xlogerr'][indx]**2,measured_data['xycovar'][indx]], \
                                                                   [measured_data['xycovar'][indx],measured_data['ylogerr'][indx]**2]])

        #Then, shift the ydata according to the new scaling relation.
        mock_ylog[indx] += mock_xlog[indx]*scr_in['beta']['bf'] + scr_in['alpha']['bf'] + npr.randn(1)*scr_in['sigma']['bf']

    return mock_xlog, mock_ylog

def find_linmixerr_bias(measured_data,scr_uncorr=False,run_id=''):
    #This is the proper structure of the input data.
    #measured_data = {xlog:xlog,ylog:ylog,xlogerr:xlogerr,ylogerr:ylogerr,xycovar:xycovar}
    xlog = measured_data['xlog']
    ylog = measured_data['ylog']
    xlogerr = measured_data['xlogerr']
    ylogerr = measured_data['ylogerr']
    xycovar = measured_data['xycovar']
    tic = time.time()
    overwrite_fits = True#False

    if overwrite_fits:
        overwrite_string = 'full_'
    else:
        overwrite_string = 'post_'

    #This string serves as a unique identified in case you want to change some parameters while the previous version of the program is still running.
    

    # Set up the plotting
    #if pdf_plot is None:
    #    plot_filename = dir_config.sr_plot_folder + \
    #    overwrite_string + 'linmix_err_characterization_'+date.today().isoformat()+run_id + '.pdf'
    #    pdf_plot = PdfPages(plot_filename)


    # First put together the fiducial struct characterizing the sample.
    if scr_uncorr is False:

        scr_uncorr = {'alpha':{'bf':0.0,'sig': {'median' : 0.5, 'upper' : 0.5, 'lower' : 0.5}}, \
                      'beta':{'bf':0.0,'sig': {'median' : 0.5, 'upper' : 0.5, 'lower' : 0.5}}, \
                      'sigma':{'bf':0.1,'sig': {'median' : 0.1, 'upper' : 0.1, 'lower' : 0.1}}}

    #We'll take the median over all iterations and save the output values to the following fits file.
    fid_string = 'linmix_beta_' + '{:.2f}'.format(scr_uncorr['beta']['bf']) + \
                 '_alpha_' + '{:.2f}'.format(scr_uncorr['alpha']['bf']) + \
                 '_sigma_' + '{:.2f}'.format(scr_uncorr['sigma']['bf'])

    #This saves all of the (?)
    big_fileout = dir_config.mockCatFolder + overwrite_string + fid_string + run_id + '.fits'

    #This is the same as big_filout, but it is more convenient to load from within python.
    big_pickle_out = dir_config.mockCatFolder + overwrite_string + fid_string + run_id + '.p'

    if overwrite_fits:

        # This will be the struct where we change the input parameters on.
        scr_in = deepcopy(scr_uncorr)

        # Choose the number of times that you want to iterate over each sample.
        niterate = 20#00

        #Choose the absolute number of steps to do before forcing the program to close...
        max_steps = 4#0
        nsteps = 0

        #Now, we will start cycling until we reach a good enough measurement.
        while nsteps < max_steps:

            #This can be moved to the end of the while loop once the program is finalized.
            nsteps+=1
            print('\nRunning step: ' + str(nsteps) + ' will halt after ' + str(max_steps) + \
                  ' if the fitting doesn\'t converge...')

            # Then, save these values to a file
            # I think that I should save this as a fits file.
            file_string = 'beta_' + '{:.2f}'.format(scr_in['beta']['bf']) + '_alpha_' + \
                          '{:.2f}'.format(scr_in['alpha']['bf']) + \
                          '_sigma_' + '{:.2f}'.format(scr_in['sigma']['bf'])
                
            #filename = dir_config.mockCatFolder + 'sr_' + file_string + run_id + '.fits'
            filename = dir_config.mockCatFolder + 'sr_temp' + run_id + '.fits'
            #fileout = dir_config.mockCatFolder + 'bf_' + file_string + run_id + '.fits'
            fileout = dir_config.mockCatFolder + 'bf_temp' + run_id + '.fits'
            
            print('Will save the individual mock catalogs to: ' + filename)

            iter_scr_out_a = []#np.empty(niterate)
            iter_scr_out_b = []#np.empty(niterate)
            iter_scr_out_s = []#np.empty(niterate)

            for j in range(niterate):

                print('\nStep: ' + str(nsteps) + ' will halt after ' + str(max_steps) + \
                      ' if the fitting doesn\'t converge...')

                print('\nIteration: ' + str(j) + ' of ' + str(niterate) + '.\n')
                # First, generate the random catalog.
                mock_xlog, mock_ylog = generate_mocks_from_measured_data(measured_data,scr_in = scr_in)

                post = run_linmixerr_idl(mock_xlog,mock_ylog,xlogerr,ylogerr,xycov = xycovar,run_id = run_id)
                scr_out =  find_linmixerr_uncertainty(post)

                alpha_fit = (scr_out['alpha']['bf'] - scr_uncorr['alpha']['bf'])/scr_uncorr['alpha']['sig']['median']
                beta_fit = (scr_out['beta']['bf'] - scr_uncorr['beta']['bf'])/scr_uncorr['beta']['sig']['median']
                sigma_fit = (scr_out['sigma']['bf'] - scr_uncorr['sigma']['bf'])/scr_uncorr['sigma']['sig']['median']
                
                print('The program has been running for '+'{:.2f}'.format((time.time()-tic)/60.0)+' minutes....')
                print('Delta [alpha, beta, sigma]: [' + '{:.2f}'.format(alpha_fit) + ', ' + \
                      '{:.2f}'.format(beta_fit) + ', ' + '{:.2f}'.format(sigma_fit) + ']')

                iter_scr_out_a.append(scr_out['alpha']['bf'])
                iter_scr_out_b.append(scr_out['beta']['bf'])
                iter_scr_out_s.append(scr_out['sigma']['bf'])
            #
            #Shift scr_in by the iterations
            alpha_fit = (scr_out['alpha']['bf'] - scr_uncorr['alpha']['bf'])/scr_uncorr['alpha']['sig']['median']
            beta_fit = (scr_out['beta']['bf'] - scr_uncorr['beta']['bf'])/scr_uncorr['beta']['sig']['median']
            sigma_fit = (scr_out['sigma']['bf'] - scr_uncorr['sigma']['bf'])/scr_uncorr['sigma']['sig']['median']

            if alpha_fit < 0.1 and  beta_fit < 0.1 and  sigma_fit < 0.1:

                #print('\nfind_linmixerr_bias has reached the desired level of precision, ending the measurements....')
                #print('\a')
                #interact(local=locals())
                nsteps = max_steps

            else:

                scr_mock_in = deepcopy(scr_in)
                scr_in['alpha']['bf'] += scr_uncorr['alpha']['bf'] - np.median(iter_scr_out_a)
                scr_in['beta']['bf'] += scr_uncorr['beta']['bf'] - np.median(iter_scr_out_b) 
                scr_in['sigma']['bf'] += scr_uncorr['sigma']['bf'] - np.median(iter_scr_out_s)

            #END OF STEPS
            #KEEP linmix_data = plot_linmix_err_idl(scr_in,fileout)
            #KEEP junk = plot_mock_catalog(xlog, ylog, xlogerr, ylogerr, scr_in, linmix_data)
        all_fits = {'alpha':np.array(iter_scr_out_a),'beta':np.array(iter_scr_out_b),'sigma':np.array(iter_scr_out_s)}
        #junk = plot_linmix_iterations(all_fits,scr_in,scr_uncorr)
        scr_corr = deepcopy(scr_in)

    if False:

        print(scr_in['beta']['bf'])
        print(scr_uncorr['beta']['bf'])
        print(scr_corr['beta']['bf'])

        print('\nfind_linmixerr_bias make sure that it stops here and see what it does....')
        print('\a')
        interact(local=locals())

    return [scr_corr,scr_mock_in,all_fits]

def find_linmixerr_uncertainty(post):

    alpha = post['alpha']
    alpha.sort()
    beta = post['beta']
    beta.sort()
    sigma = post['sigma']
    sigma.sort()

    npts = float(len(beta))
    median = int(npts*0.5)
    one_sigma_lower = int(npts*0.1586)
    one_sigma_upper = int(npts*0.8413)
    two_sigma_lower = int(npts*0.0227)
    two_sigma_upper = int(npts*0.9772)

    bf_sr = {'alpha':{'bf':alpha[median], \
                      'sig':{'median':0.5*(alpha[one_sigma_upper] - alpha[one_sigma_lower]), \
                             'upper':alpha[one_sigma_upper] - alpha[median], \
                             'lower':alpha[median] - alpha[one_sigma_lower]}}, \
             'beta':{'bf':beta[median], \
                      'sig':{'median':0.5*(beta[one_sigma_upper] - beta[one_sigma_lower]), \
                             'upper':beta[one_sigma_upper] - beta[median], \
                             'lower':beta[median] - beta[one_sigma_lower]}}, \
             'sigma':{'bf':sigma[median], \
                      'sig':{'median':0.5*(sigma[one_sigma_upper] - sigma[one_sigma_lower]), \
                             'upper':sigma[one_sigma_upper] - sigma[median], \
                             'lower':sigma[median] - sigma[one_sigma_lower]}}}

    return bf_sr

#Given that some fits of linmixerr are catastrophically wrong. 
#The purpose of this function is to iterate several times over linmixerr, remove the outliers, and obtain the median...
def run_linmixerr_iterate(xlog,ylog,xlogerr,ylogerr,xycov=None,niter=5,run_id=''):

    iter_scr_out_a = []
    iter_scr_out_b = []
    iter_scr_out_s = []
    scr_all = []
    posts_all = []

    for nx in range(niter):

        print('\nIteration: ' + str(nx) + ' over ' + str(niter) + ' independent fits...\n')
        post = run_linmixerr_idl(xlog,ylog,xlogerr,ylogerr,xycov=xycov,run_id=run_id)
        scr_out = find_linmixerr_uncertainty(post)

        iter_scr_out_a.append(scr_out['alpha']['bf'])
        iter_scr_out_b.append(scr_out['beta']['bf'])
        iter_scr_out_s.append(scr_out['sigma']['bf'])
        scr_all.append(scr_out)
        posts_all.append(post)

    scr_all = np.array(scr_all)
    posts_all = np.array(posts_all)
    all_fits = {'alpha':np.array(iter_scr_out_a),\
                'beta':np.array(iter_scr_out_b),\
                'sigma':np.array(iter_scr_out_s)}

    #Find the median absoluate deviation in order to identify the outliers.
    med_dist = np.abs(all_fits['beta'] - np.median(all_fits['beta']))
    mad = np.median(med_dist)
    mad_index = med_dist/mad
    good_mad = np.where(mad_index < 2)

    all_fits['alpha'] = all_fits['alpha'][good_mad]
    all_fits['beta'] = all_fits['beta'][good_mad]
    all_fits['sigma'] = all_fits['sigma'][good_mad]
    scr_all = scr_all[good_mad]
    posts_all = posts_all[good_mad]

    med_dist = np.abs(all_fits['beta'] - np.median(all_fits['beta']))
    good_mad = np.argmin(med_dist)
    scr_out = scr_all[good_mad]
    post = posts_all[good_mad]
    
    return [post,scr_out]

def run_linmixerr_idl(xlog,ylog,xlogerr,ylogerr,xycov=None,run_id=''):

    ## First, insert that data into a temporary fits file.
    if xycov is None:
        xycov = xlog*0.0

    col1 = fits.Column(name = 'xlog', format = 'F', array = xlog)
    col2 = fits.Column(name = 'ylog', format ='F', array = ylog)
    col3 = fits.Column(name = 'xlogerr', format ='F', array = xlogerr)
    col4 = fits.Column(name = 'ylogerr', format = 'F', array = ylogerr)
    col5 = fits.Column(name = 'xycov', format = 'F', array = xycov)
        
    #cols = fits.ColDefs([col1, col2, col3, col4])
    cols = fits.ColDefs([col1, col2, col3, col4, col5])

    tbhdu = fits.BinTableHDU.from_columns(cols)
    filename = dir_config.mockCatFolder + 'sr_temp' + run_id + '.fits'
    tbhdu.writeto(filename, clobber = True)

    #Then, create an idl instance and run linmixerr with that. 
    idl = pidly.IDL()
    post = idl.func('run_linmixerr',filename)

    all_alpha = np.array([postx['alpha'].tolist() for postx in post])
    all_beta = np.array([postx['beta'].tolist() for postx in post])
    all_sigsqr = np.array([postx['sigsqr'].tolist() for postx in post])
    
    post = {'alpha':all_alpha,'beta':all_beta,'sigma':np.sqrt(all_sigsqr)}
    #This is the old function
    #idl = pidly.IDL()
    #output = idl.func('characterize_linmixerr',scr_in['beta'],scr_in['alpha'],scr_in['sigma'])
    #print('\nfinished with run_linmix_err_idl\n')
    
    return post

def run_linfitex_idl(xlog,ylog,xlogerr,ylogerr,betafix,run_id=''):
    #This provides an option to do the best-fit for a fixed slope, clearly, the results should not be over interpreted. 
    # First, insert that data into a temporary fits file.
    ## First, create a header, which we will save to the primary HDU
    prihdr = fits.Header()
    prihdr['betafix'] = betafix
    prihdu = fits.PrimaryHDU(header = prihdr)

    ## Second, generate the columns. 
    col1 = fits.Column(name = 'xlog', format = 'F', array = xlog)
    col2 = fits.Column(name = 'ylog', format ='F', array = ylog)
    col3 = fits.Column(name = 'xlogerr', format ='F', array = xlogerr)
    col4 = fits.Column(name = 'ylogerr', format = 'F', array = ylogerr)
        
    cols = fits.ColDefs([col1, col2, col3, col4])

    tbhdu = fits.BinTableHDU.from_columns(cols)

    tbhdulist = fits.HDUList(([ prihdu, tbhdu]))
    filename = dir_config.mockCatFolder + 'sr_temp' + run_id + '.fits'
    tbhdulist.writeto(filename, clobber = True)


    #Then, create an idl instance and run linmixerr with that. 
    idl = pidly.IDL()
    best_fit = idl.func('run_linfitex',filename)

    bf_sr = {'alpha':{'bf':best_fit['intercept'], \
                      'sig':{'median':best_fit['d_intercept'], \
                             'upper':best_fit['d_intercept'], \
                             'lower':best_fit['d_intercept']}}, \
             'beta':{'bf':best_fit['slope'], \
                      'sig':{'median':best_fit['d_slope'], \
                             'upper':best_fit['d_slope'], \
                             'lower':best_fit['d_slope']}}, \
             'sigma':{'bf':best_fit['scatter'], \
                      'sig':{'median':0.0, \
                             'upper':0.0, \
                             'lower':0.0}}}

    #sr_bf = {slope:betafix,intercept:SRfit[0],scatter:sigma_i,d_slope:0.0,d_intercept:p[0]} 
    #all_alpha = np.array([postx['alpha'].tolist() for postx in post])
    #all_beta = np.array([postx['beta'].tolist() for postx in post])
    #all_sigsqr = np.array([postx['sigsqr'].tolist() for postx in post])
    
    #post = {'alpha':all_alpha,'beta':all_beta,'sigma':np.sqrt(all_sigsqr)}
    #This is the old function
    #idl = pidly.IDL()
    #output = idl.func('characterize_linmixerr',scr_in['beta'],scr_in['alpha'],scr_in['sigma'])
    #print('\nfinished with run_linmix_err_idl\n')

    return bf_sr

def get_linmixerr_contours(xlims,post):


    #Make 100 steps
    step_x = (xlims[1] - xlims[0])*0.01
    contour_x = np.arange(xlims[0],xlims[1]+step_x,step_x)

    #This should already have been take care of.
    #alpha = np.array(post['alpha'])
    #beta = np.array(post['beta'])
    alpha = np.array(post['alpha'])
    beta = np.array(post['beta'])

    y_1sig_upper = []
    y_1sig_lower = []
    y_2sig_upper = []
    y_2sig_lower = []

    npts = float(len(beta))
    one_sigma = npts*np.array([0.1586,0.8413])
    two_sigma = npts*np.array([0.0227,0.9772])

    for temp_x in contour_x:

        temp_y = beta*temp_x + alpha
        temp_y.sort()        
        y_1sig_upper.append(temp_y[int(one_sigma[1])])
        y_1sig_lower.append(temp_y[int(one_sigma[0])])
        y_2sig_upper.append(temp_y[int(two_sigma[1])])
        y_2sig_lower.append(temp_y[int(two_sigma[0])])

    contour_y = {'1sig':{'upper':np.array(y_1sig_upper),'lower':np.array(y_1sig_lower)}, \
                 '2sig':{'upper':np.array(y_2sig_upper),'lower':np.array(y_2sig_lower)}}

    return [contour_x,contour_y]

# The following functions have yet to be converted in order for them to be able to be called from external programs.
def generate_mock_catalog(scr_in = None):

    # Set the default value for the scaling relation.
    if scr_in is None:
        scr_in = {'npts':10,\
               'beta':1.6,'alpha':0.1,'sigma':0.1, \
               'delta_b':0.1,'delta_a':0.05,'delta_s':0.05, \
               'xrange':1, 'xerr_m':0.15,'xerr_std':0.05, \
               'yerr_m':0.15,'yerr_std':0.05}

    #Returns values in [0.0,1.0)
    xlog = npr.random(scr_in['npts']) * scr_in['xrange']
    xlogerr = scr_in['xerr_std'] * npr.randn(scr_in['npts']) + scr_in['xerr_m']
    xlog += xlogerr * npr.randn(scr_in['npts']) 

    ylog = scr_in['beta'] * xlog + scr_in['alpha'] + scr_in['sigma'] * npr.randn(scr_in['npts'])
    ylogerr = scr_in['yerr_std'] * npr.randn(scr_in['npts']) + scr_in['yerr_m']
    ylog += ylogerr * npr.randn(scr_in['npts'])     

    return xlog, ylog, xlogerr, ylogerr

def save_data_pts_to_file(xlog,ylog,xlogerr,ylogerr,scr_in,filename):
    
    ## First, create a header, which we will save to the primary HDU
    prihdr = fits.Header()
    prihdr['npts'] = scr_in['npts']
    prihdr['beta'] = scr_in['beta']
    prihdr['alpha'] = scr_in['alpha']
    prihdr['sigma'] = scr_in['sigma']
    prihdr['xrange'] = scr_in['xrange']
    prihdr['xerr_m'] = scr_in['xerr_m']
    prihdr['xerr_std'] = scr_in['xerr_std']
    prihdr['yerr_m'] = scr_in['yerr_m']
    prihdr['yerr_std'] = scr_in['yerr_std']

    prihdu = fits.PrimaryHDU(header = prihdr)

    ## Then, insert that data here.
    col1 = fits.Column(name = 'xlog', format = 'F', array = xlog)
    col2 = fits.Column(name = 'ylog', format ='F', array = ylog)
    col3 = fits.Column(name = 'xlogerr', format ='F', array = xlogerr)
    col4 = fits.Column(name = 'ylogerr', format = 'F', array = ylogerr)
    cols = fits.ColDefs([col1, col2, col3, col4])

    tbhdu = fits.BinTableHDU.from_columns(cols)

    tbhdulist = fits.HDUList(([ prihdu, tbhdu]))
    tbhdulist.writeto(filename, clobber = True)

    return

def linmixerr_bias_correction(scr_uncorr,big_pickle_out):

    #Load pickle file
    [all_scr_in_a, all_scr_in_b, all_scr_in_s,all_scr_out_a, all_scr_out_b, all_scr_out_s,fid_scr] = \
                                                                      pickle.load(open(big_pickle_out,"rb"))

    # Find the closest match
    a_x = np.abs(all_scr_out_a[:,0,0] - scr_uncorr['alpha']).argmin()
    a_match = all_scr_out_a[a_x,0,0]

    b_x = np.abs(all_scr_out_b[0,:,0] - scr_uncorr['beta']).argmin()
    b_match = all_scr_out_b[0,b_x,0]

    s_x = np.abs(all_scr_out_s[0,0,:] - scr_uncorr['sigma']).argmin()
    s_match = all_scr_out_s[0,0,s_x]

    if (np.abs(a_match - scr_uncorr['alpha']) > scr_uncorr['delta_a']) or \
       (np.abs(b_match - scr_uncorr['beta']) > scr_uncorr['delta_b']) or \
       (np.abs(s_match - scr_uncorr['sigma']) > scr_uncorr['delta_s']):

        print(['alpha:','{:.2f}'.format(all_scr_out_a[:,0,0].min()), \
               '{:.2f}'.format(all_scr_out_a[:,0,0].max()), \
               '{:.2f}'.format(scr_uncorr['alpha'])])
        print(['beta:','{:.2f}'.format(all_scr_out_b[0,:,0].min()), \
               '{:.2f}'.format(all_scr_out_b[0,:,0].max()), \
               '{:.2f}'.format(scr_uncorr['beta'])])
        print(['sigma:','{:.2f}'.format(all_scr_out_s[0,0,:].min()), \
               '{:.2f}'.format(all_scr_out_s[0,0,:].max()), \
               '{:.2f}'.format(scr_uncorr['sigma'])])

        print('\nYour fit is beyond the range probed by our mock samples, ' + \
              'please extend the range of the tests to include your best fit.')
        interact(local=locals())

    scr_corr = copy(scr_uncorr)
    scr_corr['alpha'] = scr_uncorr['alpha'] + (all_scr_in_a[a_x,0,0] - a_match)
    scr_corr['beta'] = scr_uncorr['beta'] + (all_scr_in_b[0,b_x,0] - b_match)
    scr_corr['sigma'] = scr_uncorr['sigma'] + (all_scr_in_s[0,0,s_x] - s_match)
    print("\n[a_out, a_in, a_uncorr, a_corr]")
    print(['{:.2f}'.format(a_match),'{:.2f}'.format(all_scr_in_a[a_x,0,0]), \
           '{:.2f}'.format(scr_uncorr['alpha']),'{:.2f}'.format(scr_corr['alpha'])])
    print("\n[b_out, b_in, b_uncorr, b_corr]")
    print(['{:.2f}'.format(b_match),'{:.2f}'.format(all_scr_in_b[0,b_x,0]), \
           '{:.2f}'.format(scr_uncorr['beta']),'{:.2f}'.format(scr_corr['beta'])])
    print("\n[s_out, s_in, s_uncorr, s_corr]")
    print(['{:.2f}'.format(s_match),'{:.2f}'.format(all_scr_in_s[0,0,s_x]), \
           '{:.2f}'.format(scr_uncorr['sigma']),'{:.2f}'.format(scr_corr['sigma'])])

    return scr_corr

def plot_linmix_err_idl(scr_in,fileout,pdf_plot=None):

    hdulist = fits.open(fileout)
    linmix_data = hdulist[1]
    alpha = linmix_data.data['ALPHA']
    beta = linmix_data.data['BETA']
    sigsqr = linmix_data.data['SIGSQR']

    scale_x = 0.5
    med_x = np.median(alpha)
    xlims = np.array([min(alpha)-scale_x,max(alpha)+scale_x])
    scale_y = 0.5
    med_y = np.median(beta)
    ylims = np.array([min(beta)-scale_y,max(beta)+scale_y])
    plt.xlim(xlims)
    plt.ylim(ylims)

    plt.plot(xlims,[scr_in['beta'],scr_in['beta']])#,label='Input SR')
    plt.plot([scr_in['alpha'],scr_in['alpha']],ylims)#,label='Input SR')
    plt.plot(xlims,[med_y,med_y],'r--')#,label='Input SR')
    plt.plot([med_x,med_x],ylims,'r--')#,label='Input SR')
    plt.scatter(alpha,beta,marker='.')
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$\beta$')
    plt.title('Linmixerr Results.')

    if pdf_plot is None:
        plt.show()
    else:
        pdf_plot.savefig()

    return linmix_data

def plot_linmix_iterations(all_fits,sr_corr, sr_uncorr, sr_mock_in,pdf_plot=None):

    all_keys = ['alpha','beta','sigma']
    key_labels = [r'$\alpha$',r'$\beta$',r'$\sigma$']

    f, all_x = plt.subplots(1,3,sharey=False,figsize=(9,6))
    plt.suptitle('  Output from mock fitting')#,sharex=False,

    for item in range(len(all_keys)):

        all_x[item].ticklabel_format(style='sci',axis='x',scilimits=(-1,1))
        all_x[item].xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
        all_x[item].locator_params(axis='x',nbins=4)
        hist_out = all_x[item].hist(all_fits[all_keys[item]])
        all_x[item].set_xlabel(key_labels[item])
        item_in = sr_mock_in[all_keys[item]]
        item_corr = sr_corr[all_keys[item]]
        item_uncorr = sr_uncorr[all_keys[item]]
        all_x[item].axvline(item_in['bf'],color='b',ls='--',label='mock in')
        all_x[item].axvline(item_corr['bf'],color='r',ls='-.',label='data corr')
        all_x[item].axvline(item_uncorr['bf'],color='g',ls=':',label='data uncorr')
        all_vals = np.append([item_in['bf'],item_corr['bf']],all_fits[all_keys[item]])
        x_min = min(all_vals)
        x_max = max(all_vals)
        x_delta = 0.1*(x_max - x_min)
        all_x[item].set_xlim([x_min-x_delta,x_max+x_delta])
        y_max = max(hist_out[0])*1.5
        all_x[item].set_ylim([0,y_max])

    if pdf_plot is None:
        print('\a')
        print('Make sure that this is plotted correctly.')
        plt.show()
    else:
        plt.legend()
        pdf_plot.savefig()
    return 

def plot_final_linmixerr_bias(big_pickle_out):

    # Load the output pickle file
    [all_scr_in_a, all_scr_in_b, all_scr_in_s,all_scr_out_a, all_scr_out_b, all_scr_out_s,fid_scr] = pickle.load(open(big_pickle_out,"rb"))

    all_scr_in = [all_scr_in_a, all_scr_in_b, all_scr_in_s]
    all_scr_out = [all_scr_out_a, all_scr_out_b, all_scr_out_s]

    in_titles = [r'Intercept: $\alpha_{in}$',r'Slope: $\beta_{in}$',r'Intrinsic Scatter: $\sigma_{in}$']
    out_titles = [r'$\Delta$ Intercept: $\alpha_{out} - \alpha_{in}$', \
                  r'$\Delta$ Slope: $\beta_{out}-\beta{in}$', \
                  r'$\Delta$ Intrinsic Scatter: $\sigma_{out}-\sigma_{in}$']

    # Cycle through the various values of the intrinsic scatter first
    plot_linmixerr_contours(all_scr_in,all_scr_out,in_titles,out_titles)

    return
