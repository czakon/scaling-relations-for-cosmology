# Usage: this code only needs to be run once.
# python setup.py develop --user
#
# MODIFICATION HISTORY:
# 11/22/2015: Created by Nicole Czakon
#
# packages=['fit_data']
#
# USAGE:
# from clusters import fit_data as fd

from setuptools import setup
from setuptools import find_packages

setup(name='fit_data', \
      description = 'this package includes all of the scripts that I have written to fit the data.', \
      author = 'Nicole Czakon', \
      packages = find_packages(), \
      zip_sage = False)


