#This is my first attempt at making a python package, proceed with caution.
#Usage: this code only needs to be run one.
# python setup.py install --user
#07/04/2015 Created by Nicole Czakon
#
#      packages=['load_data','configuration_files'], \
from setuptools import setup
from setuptools import find_packages

setup(name='cosmology', \
      description = 'this package contains all of the directories for the things that I need to run the cosmology with.', \
      author='Nicole Czakon', \
      packages=find_packages(), \
      zip_sage=False)


